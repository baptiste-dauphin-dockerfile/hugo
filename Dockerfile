FROM ubuntu:19.10

# Dependencies
RUN apt-get update \
&& apt-get install -y git golang-go

# Install hugo from sources
RUN git clone https://github.com/gohugoio/hugo.git /opt/hugo \
&& cd /opt/hugo \
&& git checkout v0.67.0 \
&& go install -i \
&& echo 'Verify hugo version' \
&& /root/go/bin/hugo version

# Clean
RUN apt-get purge git golang-go -y \
&& apt-get autoremove -y \ 
&& apt-get clean \
&& apt-get autoclean \
&& rm -rf /var/lib/apt/lists/* \
&& rm -rf /root/go/pkg/mod \
&& rm -rf /root.cache

ENV PATH /root/go/bin:$PATH

# All commands will be prefixed by the ENTRYPOINT
ENTRYPOINT ["hugo"]
# If you provide nothing, it will fallback on the following CMD
CMD ["version"]