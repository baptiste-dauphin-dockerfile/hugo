# hugo

## Docker build & push
```bash
docker build -t baptistedauphin/hugo:0.67.0 . \
&& docker push baptistedauphin/hugo:0.67.0
```


## Dirty draft
Intended to be used for a Hugo project like the following !

```bash
git clone https://github.com/Somrat37/somrat.git
cd somrat/exampleSite/
/root/go/bin/hugo --themesDir ../..
```

